public class Main {
    public static void main(String[] args) {
        Figure square = new Square(15,20, 25);
        Figure circle = new Circle(18,16,25);
        Figure ellipse = new Ellipse(10,15,25,18);
        Figure rectangle = new Rectangle(11,13, 25, 16);
        square.getPerimeter();
        circle.getPerimeter();
        ellipse.getPerimeter();
        rectangle.getPerimeter();
    }
}
