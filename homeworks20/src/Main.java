import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

Используя Java Stream API, вывести:

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *

 */
public class Main {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
            reader.lines()
                    .filter(line -> line.contains("черный") || line.contains("|0"))
                    .map(line -> {
                        List<String> list = new ArrayList<>();
                        String[] array = line.split("\\n");
                        for (String s : array) {
                            String ss = s.substring(0, s.indexOf('|'));
                            list.add(ss);
                        }
                        return list;
                    })
                    .forEach(line -> System.out.println("номер: " + line));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
