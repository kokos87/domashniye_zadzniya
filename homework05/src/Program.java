public class Program {
    public static void main(String[] args) {
        int[] intMas = {345, 155, 0, 456, -1};
        int i = 0;
        int min = 9;
        while (intMas[i] != -1) {
            int x = intMas[i];
            if (x == 0) {
                min = 0;
            } else {
                while (x != 0) {
                    int k = x % 10;
                    if (k < min) {
                        min = k;
                    }
                    x /= 10;
                }
            }
            i++;
        }
        System.out.println("Minimum = " + min);
    }
}
